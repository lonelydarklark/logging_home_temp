require 'pi_piper'
require './insertDb.rb'

def getTemp
  value = 0
  PiPiper::Spi.begin do |spi|
    raw = spi.write [0b01101000, 0]
    value = ((raw[0]<<8) + raw[1]) & 0x03FF
  end
  volt = (value * 3300)/1024
  degree = (volt - 500) /10

  return degree
end

temp = getTemp()
puts temp
now = Time.now.strftime("%Y/%m/%d\t%H:%M:%S")
open("tempLog_#{Time.now.year}#{"%02d"%Time.now.month}.txt", "a") do |io|
  io.puts("#{now}\t#{temp}")
end

begin
insertTempLog now.gsub("/","-").gsub("\t"," "), temp
rescue => e
    puts e
end
