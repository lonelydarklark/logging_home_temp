require 'pg'
require 'yaml'
require 'pp'

def getSensorDbConnect
    dbconf = YAML.load_file("./database.yml")["db"]["sensor_db"]
    return PG::connect(dbconf)

rescue => e
    return nil
end

def insertTempLog time, temp

    connect = getSensorDbConnect
    result = connect.exec("INSERT INTO temp_log (date_time, temparature) values ('#{time}', #{temp})")
    pp result
ensure
    connect.close if connect
end

=begin
open("tempLog_201602.txt") do |io|
    lines = io.read.split("\n")

    lines.each do |line|
        l = line.split("\t")
        begin
        insertTempLog l[0].gsub("/","-") + " " + l[1], l[2]
        rescue => e
            puts e
        end
    end
end
=end
