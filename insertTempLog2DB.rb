require './insertDb.rb'

open("tempLog_20163.txt") do |io|
    lines = io.read.split("\n")

    lines.each do |line|
        l = line.split("\t")
        begin
        insertTempLog l[0].gsub("/","-") + " " + l[1], l[2]
        rescue => e
            puts e
        end
    end
end
